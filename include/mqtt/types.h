#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>
#include <stdint.h>

#define MQTT_VBI32_SIZE 4u /**< Maximum possible number of bytes. */

/**
 * Variable byte integer as specified by 1.5.5
 */
struct mqtt_vbi32
{
	uint8_t bytes[MQTT_VBI32_SIZE]; /**< Bytes holding integer value. */
	uint8_t size; /**< Number of bytes used to hold the integer value. */
};

/**
 * 1.5.5
 * Encodes a given non-negative integer into a variable byte integer.
 * \param x Non-negative integer.
 * \param vbi VBI.
 * \return 0 on success, otherwise 1.
 */
uint8_t mqtt_vbi32_enc(uint32_t const x, struct mqtt_vbi32 vbi[static 1]);

/**
 * 1.5.5
 * Decodes variable byte integer into a non-negative integer.
 * \param vbi VBI.
 * \param x Non-negative integer.
 * \return 0 on success, otherwise 1.
 */
uint8_t mqtt_vbi32_dec(struct mqtt_vbi32 const vbi, uint32_t x[static 1]);

/**
 * UTF-8 encoded string as specified by 1.5.4
 */
struct mqtt_str
{
	char *data; /**< String. */
	uint16_t size; /**< String length. */
};

/**
 * Set the string to the given value of the specified type.
 * User is responsible of handling the passed data structure e.g., freeing it.
 */
uint8_t mqtt_str_set(struct mqtt_str str[static 1], uint16_t sz, char s[sz]);

uint8_t mqtt_str_deinit(struct mqtt_str str[static 1]);

/**
 * UTF-8 string pair as specified by 1.5.7
 */
struct mqtt_strpair
{
	struct mqtt_str *name; /**< String key. */
	struct mqtt_str *value; /**< String value. */
};

uint8_t mqtt_strpair_set(struct mqtt_strpair strpair[static 1],
	struct mqtt_str name[static 1], struct mqtt_str value[static 1]);

uint8_t mqtt_strpair_deinit(struct mqtt_strpair strpair[static 1]);

/**
 * Binary data as specified by 1.5.6
 */
struct mqtt_bin
{
	uint8_t *data; /**< Data. */
	uint16_t size; /**< Data size. */
};

uint8_t mqtt_bin_set(
	struct mqtt_bin bin[static 1], uint16_t sz, uint8_t data[sz]);
uint8_t mqtt_bin_deinit(struct mqtt_bin bin[static 1]);

/**
 * Possible data representations as specified in 1.5
 */
enum mqtt_data_t
{
	MQTT_DATA_U8_T = 0, /**< Single byte. */
	MQTT_DATA_U16_T, /**< Two bytes. */
	MQTT_DATA_U32_T, /**< Four bytes. */
	MQTT_DATA_VBI32_T, /**< Variable byte integer. */
	MQTT_DATA_STR_T, /**< String. */
	MQTT_DATA_STRPAIR_T, /**< Pair of string. */
	MQTT_DATA_BIN_T, /**< Binary data. */
	MQTT_DATA_SIZE /**< Number of possible data representations/types. */
};

/**
 * Generic data representation as specified in 1.5
 */
struct mqtt_data
{
	enum mqtt_data_t type;
	/**
	 * See `enum mqtt_data_t`.
	 */
	union data
	{
		uint8_t u8;
		uint16_t u16;
		uint32_t u32;
		struct mqtt_vbi32 *vbi;
		struct mqtt_bin *bin;
		struct mqtt_str *str;
		struct mqtt_strpair *strpair;
	} data;
};

/**
 * Initializes data depending on the type.
 * \param data Data structure.
 * \param type Type.
 * \return 0 on success, otherwise 1.
 */
uint8_t mqtt_data_init(
	struct mqtt_data data[static 1], enum mqtt_data_t const type);

/**
 * Set data to the given value of the specified type.
 * For now, this function is guaranteed to never fail, instead it'll assert if
 * the type of the data is different from the type of the passed value.
 * \param data Data structure.
 * \param n Typed value.
 * \return 0 on success, otherwise 1.
 */
uint8_t mqtt_data_u8_set(struct mqtt_data data[static 1], uint8_t const n);

uint8_t mqtt_data_u16_set(struct mqtt_data data[static 1], uint16_t const n);

uint8_t mqtt_data_u32_set(struct mqtt_data data[static 1], uint32_t const n);

uint8_t mqtt_data_vbi_set(
	struct mqtt_data data[static 1], struct mqtt_vbi32 vbi[static 1]);

uint8_t mqtt_data_bin_set(
	struct mqtt_data data[static 1], struct mqtt_bin bin[static 1]);

uint8_t mqtt_data_str_set(
	struct mqtt_data data[static 1], struct mqtt_str str[static 1]);

uint8_t mqtt_data_strpair_set(
	struct mqtt_data data[static 1], struct mqtt_strpair strpair[static 1]);

/**
 * Get data casted to the specified type.
 * For now, this function is guaranteed to never fail, instead it'll assert if
 * the type of the data is different from the type of the passed value.
 * \param data Data structure.
 * \param n Typed value.
 * \return 0 on success, otherwise 1.
 */
uint8_t mqtt_data_u8_get(struct mqtt_data data[static 1], uint8_t n[static 1]);

uint8_t mqtt_data_u16_get(
	struct mqtt_data data[static 1], uint16_t n[static 1]);

uint8_t mqtt_data_u32_get(
	struct mqtt_data data[static 1], uint32_t n[static 1]);

uint8_t mqtt_data_vbi_get(
	struct mqtt_data data[static 1], struct mqtt_vbi32 *vbi[static 1]);

uint8_t mqtt_data_bin_get(
	struct mqtt_data data[static 1], struct mqtt_bin *bin[static 1]);

uint8_t mqtt_data_str_get(
	struct mqtt_data data[static 1], struct mqtt_str *str[static 1]);

uint8_t mqtt_data_strpair_get(struct mqtt_data data[static 1],
	struct mqtt_strpair *strpair[static 1]);

/**
 * Deinitializes data depending on the type.
 * User *must* handle the data content previously set, e.g., freeing it, still.
 * \param data Data structure.
 * \return 0 on success, otherwise 1.
 */
uint8_t mqtt_data_deinit(struct mqtt_data data[static 1]);

#ifdef __cplusplus
}
#endif
