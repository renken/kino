#include "mqtt/types.h"
#include <assert.h>
#include <mqtt/props.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

int main(void)
{
	struct mqtt_data payload_data = {0};
	struct mqtt_cntrl_prop payload_prop = {0};

	assert(!mqtt_data_init(&payload_data, MQTT_DATA_U8_T));
	assert(!mqtt_data_u8_set(&payload_data, 8));
	assert(!mqtt_cntrl_prop_init(
		&payload_prop, MQTT_PROP_PAYLOAD_FORMAT_INDCTR));
	assert(!mqtt_cntrl_prop_data_set(&payload_prop, &payload_data));
	assert(!mqtt_cntrl_prop_deinit(&payload_prop));

	char *s = malloc(strnlen("content-type", UINT16_MAX));
	assert(s);
	size_t s_sz = strnlen(s, UINT16_MAX);
	struct mqtt_str str = {0};
	struct mqtt_data contenttype_data = {0};
	struct mqtt_cntrl_prop contenttype_payload = {0};

	assert(!mqtt_str_set(&str, s_sz, s));
	assert(!mqtt_data_init(&contenttype_data, MQTT_DATA_STR_T));
	assert(!mqtt_data_str_set(&contenttype_data, &str));
	assert(!mqtt_cntrl_prop_init(
		&contenttype_payload, MQTT_PROP_CONTENT_TYPE));
	assert(!mqtt_cntrl_prop_data_set(
		&contenttype_payload, &contenttype_data));
	assert(!mqtt_cntrl_prop_deinit(&contenttype_payload));
	free(s);

	return 0;
}
