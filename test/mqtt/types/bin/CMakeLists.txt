foreach(file
		bin)
	add_executable("test_${file}"
		"${file}.c")
	target_link_libraries("test_${file}"
		PRIVATE
		${PROJECT_NAME})

	add_test("${file}" "test_${file}")
endforeach(file)
