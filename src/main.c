#include <kino/version.h>

#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
	(void)argc;
	(void)argv;
	printf("project info:\n"
	       "\tname: %s\n"
	       "\tdesc: %s\n"
	       "\tauthor: %s\n"
	       "\tversion: %s\n"
	       "\tmail: %s\n"
	       "\tcopyright: %s\n",
		KINO_NAME, KINO_DESCRIPTION, KINO_AUTHOR, KINO_VERSION,
		KINO_MAIL, KINO_COPYRIGHT);

	return EXIT_SUCCESS;
}
