if(NOT DOCTEST_FOUND)
	find_path(DOCTEST_INCLUDE_DIR
		NAMES doctest/doctest.h
		DOC "Path to doctest as in <doctest/doctest.h>.")

	if(NOT DOCTEST_INCLUDE_DIR)
		message(FATAL_ERROR "Could not find doctest.")
	endif()
	set(DOCTEST_FOUND ON CACHE BOOL "Found doctest.")
	mark_as_advanced(DOCTEST_FOUND)
	message(STATUS "Found doctest include dir: ${DOCTEST_INCLUDE_DIR}")
endif()

add_library(doctest::doctest IMPORTED INTERFACE)
set_target_properties(doctest::doctest
	PROPERTIES INTERFACE_INCLUDE_DIRECTORIES "${DOCTEST_INCLUDE_DIR}")
