#include "str.h"

uint8_t mqtt_str_set(struct mqtt_str str[static 1], uint16_t sz, char s[sz])
{
	assert(str);
	assert(s);

	str->data = s;
	str->size = sz;

	return 0;
}

uint8_t mqtt_str_deinit(struct mqtt_str str[static 1])
{
	assert(str);

	str->data = 0;
	str->size = 0;

	return 0;
}
