#include "bin.h"

uint8_t mqtt_bin_set(
	struct mqtt_bin bin[static 1], uint16_t sz, uint8_t data[sz])
{
	assert(bin);
	assert(data);

	bin->data = data;
	bin->size = sz;

	return 0;
}
uint8_t mqtt_bin_deinit(struct mqtt_bin bin[static 1])
{
	assert(bin);

	bin->data = 0;
	bin->size = 0;

	return 0;
}
