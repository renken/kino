coverage
========

When configuring with ``-DCOVERAGE:BOOL=ON`` compiled code will be instrumented
when ran. Usually by running the tests but running code any way, such as by the
main executable and manually doing things inside the program, will work.

Three targets are added as a result. Target ``coverage_report`` builds an HTML
report based upon the lines of code ran at the point of generating.

Target ``coverage_xml`` builds an XML report using the same data as
``coverage_report``. This is especially useful when reviewing GitLab merge
requests as coverage results are displayed in the file diff view. See
`https://docs.gitlab.com/ee/user/project/merge_requests/\
test_coverage_visualization.html`__.

__ https://docs.gitlab.com/ee/user/project/merge_requests/
	test_coverage_visualization.html

Lastly the target ``coverage_reset``. This target resets all coverage counters
in preparation for either running tests again or running code any other way.

Keep in mind counters are accumulative. If the counters are empty running the
tests twice, provided your tests are deterministic, will result in all count
events happening twice compared to running tests once. Generating a coverage
report after may result into a rather confusing report when you expect lines
being executed only ``x`` times but find out they have been executed ``2x``
times.
