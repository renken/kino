#include <assert.h>
#include <mqtt/types.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(void)
{
	struct mqtt_vbi32 vbi = {0};
	uint32_t x = 0;

	puts("Encoding a number less than 128");
	{
		assert(!mqtt_vbi32_enc(127, &vbi));
		assert(vbi.size == 1);
		assert(vbi.bytes[0] == 0x7F);
		assert(!mqtt_vbi32_dec(vbi, &x));
		assert(x == 127);
	}

	puts("Encoding a number less than 16384");
	{
		assert(!mqtt_vbi32_enc(16383, &vbi));
		assert(vbi.size == 2);
		assert(vbi.bytes[0] == 0xFF);
		assert(vbi.bytes[1] == 0x7F);
		assert(!mqtt_vbi32_dec(vbi, &x));
		assert(x == 16383);
	}

	puts("Encoding a number less than 2097152");
	{
		assert(!mqtt_vbi32_enc(2097151, &vbi));
		assert(vbi.size == 3);
		assert(vbi.bytes[0] == 0xFF);
		assert(vbi.bytes[1] == 0xFF);
		assert(vbi.bytes[2] == 0x7F);
		assert(!mqtt_vbi32_dec(vbi, &x));
		assert(x == 2097151);
	}

	puts("Encoding a number less than or equal to 268435455");
	{
		assert(!mqtt_vbi32_enc(268435455, &vbi));
		assert(vbi.size == 4);
		assert(vbi.bytes[0] == 0xFF);
		assert(vbi.bytes[1] == 0xFF);
		assert(vbi.bytes[2] == 0xFF);
		assert(vbi.bytes[3] == 0x7F);
		assert(!mqtt_vbi32_dec(vbi, &x));
		assert(x == 268435455);
	}

	puts("Encoding a number greater than 268435455");
	{
		assert(mqtt_vbi32_enc(268435456, &vbi));
		vbi.size = 4;
		uint8_t const bytes[4] = {0xFF, 0xFF, 0xFF, 0x80};
		memcpy(vbi.bytes, bytes, vbi.size);
		assert(mqtt_vbi32_dec(vbi, &x));
	}
}
