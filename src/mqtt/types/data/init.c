#include "data.h"

uint8_t mqtt_data_init(
	struct mqtt_data data[static 1], enum mqtt_data_t const type)
{
	assert(data);
	assert(type != MQTT_DATA_SIZE);

	data->type = type;
	data->data.u8 = 0;

	return 0;
}
