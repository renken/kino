#include "props.h"

uint8_t mqtt_cntrl_proplist_init(struct mqtt_cntrl_proplist props[static 1])
{
	assert(props);

	props->len = 0;
	props->head = 0;

	return 0;
}

uint8_t mqtt_cntrl_proplist_prop_add(struct mqtt_cntrl_proplist props[static 1],
	struct mqtt_cntrl_prop prop[static 1])
{
	assert(props);
	assert(prop);
	uint32_t i = 0;
	struct mqtt_cntrl_prop *curr = props->head;
	prop->nxt = 0;

	/* Empty list. */
	if (!curr) {
		assert(!props->len);
		props->head = prop;
		++props->len;
	}

	for (; i < props->len; ++i) {
		assert(curr);
		curr = curr->nxt;
	}

	assert(curr);
	curr->nxt = prop;
	++props->len;

	return 0;
}

uint8_t mqtt_cntrl_proplist_deinit(struct mqtt_cntrl_proplist props[static 1])
{
	assert(props);
	uint32_t i = 0;
	struct mqtt_cntrl_prop *curr = props->head;
	struct mqtt_cntrl_prop *nxt = 0;

	if (!curr) {
		assert(!props->len);
		return 0;
	}

	for (; i < props->len; ++i) {
		assert(curr);
		nxt = curr->nxt;
		mqtt_cntrl_prop_deinit(curr);
		curr = nxt;
	}

	assert(curr);
	mqtt_cntrl_prop_deinit(curr);

	props->len = 0;
	props->head = 0;

	return 0;
}
