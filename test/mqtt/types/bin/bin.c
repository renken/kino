#include <assert.h>
#include <mqtt/types.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(void)
{
	struct mqtt_bin bin = {0};
	uint8_t data[] = {1, 2, 3, 4};
	size_t size = sizeof data / sizeof data[0];
	assert(size <= UINT16_MAX);

	assert(!mqtt_bin_set(&bin, size, data));
	assert(bin.data == data);
	assert(bin.size == size);

	assert(!mqtt_bin_deinit(&bin));
	assert(!bin.data);
	assert(!bin.size);

	return 0;
}
