#include <assert.h>
#include <mqtt/types.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

int main(void)
{
	char s[] = "str test value";
	size_t sz = strnlen(s, UINT16_MAX);
	struct mqtt_str str = {0};

	assert(!mqtt_str_set(&str, sz, s));
	assert(str.size == sz);
	assert(str.data == s);
	assert((uint16_t)strlen(str.data) == sz);
	assert((uint16_t)strlen(str.data) == str.size);
	assert(!strncmp(s, str.data, sz));

	assert(!mqtt_str_deinit(&str));
	assert(!str.data);
	assert(!str.size);

	return 0;
}
