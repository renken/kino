#include "props.h"

uint8_t mqtt_cntrl_prop_init(
	struct mqtt_cntrl_prop prop[static 1], enum mqtt_cntrl_prop_id const id)
{
	assert(prop);
	assert(id != MQTT_PROP_NONE);
	assert(id != MQTT_PROP_SIZE);

	prop->id = id;
	prop->data = 0;
	prop->nxt = 0;

	return 0;
}

uint8_t mqtt_cntrl_prop_data_set(
	struct mqtt_cntrl_prop prop[static 1], struct mqtt_data data[static 1])
{
	assert(prop);
	assert(prop->id != MQTT_PROP_NONE);
	assert(data);
	assert(data->type != MQTT_DATA_SIZE);
	assert(MQTT_PROP_TYPES[prop->id] == data->type);

	prop->data = data;

	return 0;
}

uint8_t mqtt_cntrl_prop_deinit(struct mqtt_cntrl_prop prop[static 1])
{
	assert(prop);
	uint8_t e = 0;

	e = mqtt_data_deinit(prop->data);
	if (e) {
		return 1;
	}

	/* ensure that the user will properly re-init this struct if they wish
	 * to re-use it. */
	prop->id = MQTT_PROP_NONE;
	prop->data = 0;
	prop->nxt = 0;

	return 0;
}
