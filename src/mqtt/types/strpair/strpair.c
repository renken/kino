#include "mqtt/types.h"
#include "strpair.h"

uint8_t mqtt_strpair_set(struct mqtt_strpair strpair[static 1],
	struct mqtt_str name[static 1], struct mqtt_str value[static 1])
{
	assert(strpair);
	assert(name);
	assert(value);

	strpair->name = name;
	strpair->value = value;

	return 0;
}

uint8_t mqtt_strpair_deinit(struct mqtt_strpair strpair[static 1])
{
	assert(strpair);

	strpair->name = 0;
	strpair->value = 0;

	return 0;
}
