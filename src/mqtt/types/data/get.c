#include "data.h"

uint8_t mqtt_data_u8_get(struct mqtt_data data[static 1], uint8_t n[static 1])
{
	assert(data);
	assert(data->type == MQTT_DATA_U8_T);
	assert(n);

	*n = data->data.u8;

	return 0;
}

uint8_t mqtt_data_u16_get(struct mqtt_data data[static 1], uint16_t n[static 1])
{
	assert(data);
	assert(data->type == MQTT_DATA_U16_T);
	assert(n);

	*n = data->data.u16;

	return 0;
}

uint8_t mqtt_data_u32_get(struct mqtt_data data[static 1], uint32_t n[static 1])
{
	assert(data);
	assert(data->type == MQTT_DATA_U32_T);
	assert(n);

	*n = data->data.u32;

	return 0;
}

uint8_t mqtt_data_vbi_get(
	struct mqtt_data data[static 1], struct mqtt_vbi32 *vbi[static 1])
{
	assert(data);
	assert(data->type == MQTT_DATA_VBI32_T);
	assert(vbi);

	*vbi = data->data.vbi;

	return 0;
}

uint8_t mqtt_data_bin_get(
	struct mqtt_data data[static 1], struct mqtt_bin *bin[static 1])
{
	assert(data);
	assert(data->type == MQTT_DATA_BIN_T);
	assert(bin);

	*bin = data->data.bin;

	return 0;
}

uint8_t mqtt_data_str_get(
	struct mqtt_data data[static 1], struct mqtt_str *str[static 1])
{
	assert(data);
	assert(data->type == MQTT_DATA_STR_T);
	assert(str);

	*str = data->data.str;

	return 0;
}

uint8_t mqtt_data_strpair_get(
	struct mqtt_data data[static 1], struct mqtt_strpair *strpair[static 1])
{
	assert(data);
	assert(data->type == MQTT_DATA_STRPAIR_T);
	assert(strpair);

	*strpair = data->data.strpair;

	return 0;
}
