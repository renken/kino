Data representation
===================

.. contents::
	:local:

.. doxygenenum:: mqtt_data_t

.. doxygenstruct:: mqtt_data
	:members:

.. doxygenfunction:: mqtt_data_u8_set

.. doxygenfunction:: mqtt_data_u8_get

.. doxygenfunction:: mqtt_data_u16_set

.. doxygenfunction:: mqtt_data_u16_get

.. doxygenfunction:: mqtt_data_u32_set

.. doxygenfunction:: mqtt_data_u32_get

.. doxygenfunction:: mqtt_data_vbi_set

.. doxygenfunction:: mqtt_data_vbi_get

.. doxygenfunction:: mqtt_data_bin_set

.. doxygenfunction:: mqtt_data_bin_get

.. doxygenfunction:: mqtt_data_str_set

.. doxygenfunction:: mqtt_data_str_get

.. doxygenfunction:: mqtt_data_strpair_set

.. doxygenfunction:: mqtt_data_strpair_get

.. doxygenfunction:: mqtt_data_deinit

Variable byte integer
---------------------

.. doxygenstruct:: mqtt_vbi32
	:members:

.. doxygenfunction:: mqtt_vbi32_enc

.. doxygenfunction:: mqtt_vbi32_dec

String
------

.. doxygenstruct:: mqtt_str
	:members:

.. doxygenfunction:: mqtt_str_set

.. doxygenfunction:: mqtt_str_deinit

String pair
-----------

.. doxygenstruct:: mqtt_strpair
	:members:

.. doxygenfunction:: mqtt_strpair_set

.. doxygenfunction:: mqtt_strpair_deinit


Binary data
-----------

.. doxygenstruct:: mqtt_bin
	:members:

.. doxygenfunction:: mqtt_bin_set

.. doxygenfunction:: mqtt_bin_deinit
