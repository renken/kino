#include "vbi.h"
#include <assert.h>

uint8_t mqtt_vbi32_dec(struct mqtt_vbi32 const vbi, uint32_t x[static 1])
{
	uint32_t multipler = 1;
	uint8_t enc_byte;
	uint8_t i = 0;
	assert(x);
	*x = 0;

	do {
		if (i == vbi.size)
			return 1;
		enc_byte = vbi.bytes[i];
		++i;
		*x += (enc_byte & (VBI_MIN_2 - 1)) * multipler;
		if (multipler > VBI_MIN_2 * VBI_MIN_2 * VBI_MIN_2)
			return 1;
		multipler *= VBI_MIN_2;
	} while (enc_byte & VBI_MIN_2);

	return 0;
}
