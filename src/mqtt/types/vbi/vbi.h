#include <mqtt/types.h>

#define VBI_MAX 268435455u
#define VBI_MIN_4 2097152u
#define VBI_MIN_3 16384u
#define VBI_MIN_2 128u
