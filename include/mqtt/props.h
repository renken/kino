#pragma once

#include <mqtt/types.h>
#include <stddef.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

enum mqtt_cntrl_prop_id
{
	MQTT_PROP_NONE = 0U,
	MQTT_PROP_PAYLOAD_FORMAT_INDCTR,
	MQTT_PROP_MSG_EXPIRY,
	MQTT_PROP_CONTENT_TYPE,
	MQTT_PROP_RESP_TOPIC = 8U,
	MQTT_PROP_CORRELATION_DATA,
	MQTT_PROP_SUBSCRIPTION_ID = 11U,
	MQTT_PROP_SESSION_EXPIRY = 17U,
	MQTT_PROP_ASSIGNED_CLIENT_ID,
	MQTT_PROP_SERVER_KEEP_ALIVE,
	MQTT_PROP_AUTH_METHOD,
	MQTT_PROP_AUTH_DATA,
	MQTT_PROP_REQ_PROBLEM_INFO,
	MQTT_PROP_WILL_DELAY,
	MQTT_PROP_REQ_RESP_INFO,
	MQTT_PROP_RESP_INFO,
	MQTT_PROP_SERVER_REF = 28U,
	MQTT_PROP_REASON_STRING = 31U,
	MQTT_PROP_RECEIVE_MAX = 33U,
	MQTT_PROP_TOPIC_ALIAS_MAX,
	MQTT_PROP_TOPIC_ALIAS,
	MQTT_PROP_MAX_QOS,
	MQTT_PROP_RETAIN_AVAIL,
	MQTT_PROP_USER_PROP,
	MQTT_PROP_MAXPKTSIZE,
	MQTT_PROP_WILDCARD_SUB_AVAIL,
	MQTT_PROP_SUBSCRIPTION_ID_AVAIL,
	MQTT_PROP_SUBSCRIPTION_AVAIL,
	MQTT_PROP_SIZE,
};

static enum mqtt_data_t const MQTT_PROP_TYPES[MQTT_PROP_SIZE] = {
	[MQTT_PROP_NONE] = MQTT_DATA_SIZE,
	[MQTT_PROP_PAYLOAD_FORMAT_INDCTR] = MQTT_DATA_U8_T,
	[MQTT_PROP_MSG_EXPIRY] = MQTT_DATA_U32_T,
	[MQTT_PROP_CONTENT_TYPE] = MQTT_DATA_STR_T,
	[MQTT_PROP_RESP_TOPIC] = MQTT_DATA_STR_T,
	[MQTT_PROP_CORRELATION_DATA] = MQTT_DATA_BIN_T,
	[MQTT_PROP_SUBSCRIPTION_ID] = MQTT_DATA_VBI32_T,
	[MQTT_PROP_SESSION_EXPIRY] = MQTT_DATA_U32_T,
	[MQTT_PROP_ASSIGNED_CLIENT_ID] = MQTT_DATA_STR_T,
	[MQTT_PROP_SERVER_KEEP_ALIVE] = MQTT_DATA_U16_T,
	[MQTT_PROP_AUTH_METHOD] = MQTT_DATA_STR_T,
	[MQTT_PROP_AUTH_DATA] = MQTT_DATA_BIN_T,
	[MQTT_PROP_REQ_PROBLEM_INFO] = MQTT_DATA_U8_T,
	[MQTT_PROP_WILL_DELAY] = MQTT_DATA_U32_T,
	[MQTT_PROP_REQ_RESP_INFO] = MQTT_DATA_U8_T,
	[MQTT_PROP_RESP_INFO] = MQTT_DATA_STR_T,
	[MQTT_PROP_SERVER_REF] = MQTT_DATA_STR_T,
	[MQTT_PROP_REASON_STRING] = MQTT_DATA_STR_T,
	[MQTT_PROP_RECEIVE_MAX] = MQTT_DATA_U16_T,
	[MQTT_PROP_TOPIC_ALIAS_MAX] = MQTT_DATA_U16_T,
	[MQTT_PROP_TOPIC_ALIAS] = MQTT_DATA_U16_T,
	[MQTT_PROP_MAX_QOS] = MQTT_DATA_U8_T,
	[MQTT_PROP_RETAIN_AVAIL] = MQTT_DATA_U8_T,
	[MQTT_PROP_USER_PROP] = MQTT_DATA_STRPAIR_T,
	[MQTT_PROP_MAXPKTSIZE] = MQTT_DATA_U32_T,
	[MQTT_PROP_WILDCARD_SUB_AVAIL] = MQTT_DATA_U8_T,
	[MQTT_PROP_SUBSCRIPTION_ID_AVAIL] = MQTT_DATA_U8_T,
	[MQTT_PROP_SUBSCRIPTION_AVAIL] = MQTT_DATA_U8_T,
};

struct mqtt_cntrl_prop
{
	/** Although the Property Identifier is defined as a Variable Byte
	 * Integer, in this version of the specification all of the Property
	 * Identifiers are one byte long.
	 */
	enum mqtt_cntrl_prop_id id;
	/**
	 * Data passed by pointer will freed if the list is freed as well.
	 */
	struct mqtt_data *data;
	struct mqtt_cntrl_prop *nxt; /**< Next property. */
};

/**
 * Initializes the property.
 * \param prop Property.
 * \param id Property ID, see `enum mqtt_cntrl_prop_id`.
 * \return 0 on success, otherwise 1.
 */
uint8_t mqtt_cntrl_prop_init(struct mqtt_cntrl_prop prop[static 1],
	enum mqtt_cntrl_prop_id const id);

/**
 * Sets the data of the given property.
 * \param prop Property.
 * \param data Data.
 * Data representation that has to be of the same type that the property ID
 * allows/maps to. Error will be returned if so but for now assertion will take
 * place instead.
 * \return 0 on success, otherwise 1.
 */
uint8_t mqtt_cntrl_prop_data_set(
	struct mqtt_cntrl_prop prop[static 1], struct mqtt_data data[static 1]);

/**
 * Deinitializes the property.
 * Its data will also be deinitialized and the next attribute will be set to
 * NULL.
 */
uint8_t mqtt_cntrl_prop_deinit(struct mqtt_cntrl_prop prop[static 1]);

struct mqtt_cntrl_proplist
{
	/**
	 * Length of the properties.
	 * If there are no properties, this *must* be indicated by including a
	 * property length of zero.
	 */
	uint32_t len;
	struct mqtt_cntrl_prop *head; /**< First property. Can be NULL. */
};

/**
 * Initializes the properties list.
 * \param props Properties.
 */
uint8_t mqtt_cntrl_proplist_init(struct mqtt_cntrl_proplist props[static 1]);

/**
 * Adds a property to the properties list.
 * \param props Properties.
 * \param prop Property to add.
 * \return 0 on success, otherwise 1.
 */
uint8_t mqtt_cntrl_proplist_prop_add(struct mqtt_cntrl_proplist props[static 1],
	struct mqtt_cntrl_prop prop[static 1]);

/**
 * Deinitializes the properties list.
 * All properties including their data will be deinitialized as well.
 * \param props Properties.
 * \return 0 on success, otherwise 1.
 */
uint8_t mqtt_cntrl_proplist_deinit(struct mqtt_cntrl_proplist props[static 1]);

#ifdef __cplusplus
}
#endif
