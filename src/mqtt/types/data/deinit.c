#include "data.h"

uint8_t mqtt_data_deinit(struct mqtt_data data[static 1])
{
	assert(data);
	uint8_t e = 0;

	switch (data->type) {
	case MQTT_DATA_U8_T:
		/* fallthrough */
	case MQTT_DATA_U16_T:
		/* fallthrough */
	case MQTT_DATA_U32_T:
		/* fallthrough */
	case MQTT_DATA_VBI32_T:
		data->data.u8 = 0;
		break;
	case MQTT_DATA_STR_T:
		e = mqtt_str_deinit(data->data.str);
		break;
	case MQTT_DATA_STRPAIR_T:
		e = mqtt_strpair_deinit(data->data.strpair);
		break;
	case MQTT_DATA_BIN_T:
		e = mqtt_bin_deinit(data->data.bin);
		break;
	default:
		/* unreachable */
		assert(0);
	}

	if (e) {
		return 1;
	}

	/* sane value that will cause assertion failure if user does not re-init
	 * the struct properly. */
	data->type = MQTT_DATA_SIZE;
	data->data.u8 = 0;

	return 0;
}
