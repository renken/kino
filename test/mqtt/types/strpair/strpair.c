#include <assert.h>
#include <mqtt/types.h>
#include <stdint.h>
#include <string.h>

int main(void)
{
	struct mqtt_strpair strpair = {0};
	struct mqtt_str name = {0};
	struct mqtt_str value = {0};
	char name_s[] = "name";
	char name_sz = strnlen(name_s, UINT16_MAX);
	char value_s[] = "value";
	char value_sz = strnlen(value_s, UINT16_MAX);

	assert(!mqtt_str_set(&name, name_sz, name_s));
	assert(!mqtt_str_set(&value, value_sz, value_s));

	assert(!mqtt_strpair_set(&strpair, &name, &value));
	assert(strpair.name == &name);
	assert(strpair.value == &value);

	assert(!mqtt_strpair_deinit(&strpair));
	assert(!strpair.name);
	assert(!strpair.value);

	return 0;
}
