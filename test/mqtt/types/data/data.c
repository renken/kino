#include <assert.h>
#include <mqtt/types.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(void)
{
	struct mqtt_data data[MQTT_DATA_SIZE] = {0};
	enum mqtt_data_t types[MQTT_DATA_SIZE] = {
		MQTT_DATA_U8_T,
		MQTT_DATA_U16_T,
		MQTT_DATA_U32_T,
		MQTT_DATA_VBI32_T,
		MQTT_DATA_BIN_T,
		MQTT_DATA_STR_T,
		MQTT_DATA_STRPAIR_T,
	};

	for (size_t i = 0; i < MQTT_DATA_SIZE; ++i) {
		assert(!mqtt_data_init(&data[i], types[i]));
	}

	uint8_t u8 = 0;
	assert(!mqtt_data_u8_set(&data[0], 8));
	assert(!mqtt_data_u8_get(&data[0], &u8));
	assert(!mqtt_data_deinit(&data[0]));
	assert(u8 == 8);

	uint16_t u16 = 0;
	assert(!mqtt_data_u16_set(&data[1], 16));
	assert(!mqtt_data_u16_get(&data[1], &u16));
	assert(!mqtt_data_deinit(&data[1]));
	assert(u16 == 16);

	uint32_t u32 = 0;
	assert(!mqtt_data_u32_set(&data[2], 32));
	assert(!mqtt_data_u32_get(&data[2], &u32));
	assert(!mqtt_data_deinit(&data[2]));
	assert(u32 == 32);

	struct mqtt_vbi32 vbi_in = {0};
	uint32_t u32_in = 0;
	struct mqtt_vbi32 *vbi_out = 0;
	uint32_t u32_out = 0;
	assert(!mqtt_vbi32_enc(32, &vbi_in));
	assert(!mqtt_data_vbi_set(&data[3], &vbi_in));
	assert(!mqtt_data_vbi_get(&data[3], &vbi_out));
	assert(!mqtt_data_deinit(&data[3]));
	assert(!mqtt_vbi32_dec(vbi_in, &u32_in));
	assert(!mqtt_vbi32_dec(*vbi_out, &u32_out));
	assert(u32_in == u32_out);

	struct mqtt_bin bin_in = {0};
	struct mqtt_bin *bin_out = 0;
	uint8_t bytes[] = {1, 2, 3};
	uint16_t bytes_sz = sizeof bytes / sizeof bytes[0];
	assert(!mqtt_bin_set(&bin_in, bytes_sz, bytes));
	assert(!mqtt_data_bin_set(&data[4], &bin_in));
	assert(!mqtt_data_bin_get(&data[4], &bin_out));
	assert(!mqtt_data_deinit(&data[4]));
	assert(&bin_in == bin_out);

	struct mqtt_str str = {0};
	struct mqtt_str *str_out = 0;
	char s[] = "test string";
	size_t s_sz = strnlen(s, UINT16_MAX);
	assert(!mqtt_str_set(&str, s_sz, s));
	assert(!mqtt_data_str_set(&data[5], &str));
	assert(!mqtt_data_str_get(&data[5], &str_out));
	assert(!mqtt_data_deinit(&data[5]));
	assert(&str == str_out);

	struct mqtt_strpair strpair = {0};
	struct mqtt_strpair *strpair_out = 0;
	struct mqtt_str name = {0};
	char name_s[] = "name";
	size_t name_sz = strnlen(name_s, UINT16_MAX);
	struct mqtt_str value = {0};
	char value_s[] = "value";
	size_t value_sz = strnlen(value_s, UINT16_MAX);
	assert(!mqtt_str_set(&name, name_sz, name_s));
	assert(!mqtt_str_set(&value, value_sz, value_s));
	assert(!mqtt_strpair_set(&strpair, &name, &value));
	assert(!mqtt_data_strpair_set(&data[6], &strpair));
	assert(!mqtt_data_strpair_get(&data[6], &strpair_out));
	assert(!mqtt_data_deinit(&data[6]));
	assert(&strpair == strpair_out);

	return 0;
}
