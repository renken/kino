#include "vbi.h"
#include <assert.h>
#include <stdlib.h>

uint8_t mqtt_vbi32_enc(uint32_t x, struct mqtt_vbi32 vbi[static 1])
{
	uint8_t encoded;
	uint8_t i;
	assert(vbi);

	if (x > VBI_MAX) {
		return 1;
	}

	i = 0;
	do {
		encoded = x % VBI_MIN_2;
		x /= VBI_MIN_2;
		if (x)
			encoded |= VBI_MIN_2;
		vbi->bytes[i] = encoded;
		++i;
	} while (x);

	vbi->size = i;
	assert(vbi->size <= 4);

	return 0;
}
