#include "data.h"

uint8_t mqtt_data_u8_set(struct mqtt_data data[static 1], uint8_t const n)
{
	assert(data);
	assert(data->type == MQTT_DATA_U8_T);

	data->data.u8 = n;

	return 0;
}

uint8_t mqtt_data_u16_set(struct mqtt_data data[static 1], uint16_t const n)
{
	assert(data);
	assert(data->type == MQTT_DATA_U16_T);

	data->data.u16 = n;

	return 0;
}

uint8_t mqtt_data_u32_set(struct mqtt_data data[static 1], uint32_t const n)
{
	assert(data);
	assert(data->type == MQTT_DATA_U32_T);

	data->data.u32 = n;

	return 0;
}

uint8_t mqtt_data_vbi_set(
	struct mqtt_data data[static 1], struct mqtt_vbi32 vbi[static 1])
{
	assert(data);
	assert(data->type == MQTT_DATA_VBI32_T);
	assert(vbi);

	data->data.vbi = vbi;

	return 0;
}

uint8_t mqtt_data_bin_set(
	struct mqtt_data data[static 1], struct mqtt_bin bin[static 1])
{
	assert(data);
	assert(data->type == MQTT_DATA_BIN_T);
	assert(bin);

	data->data.bin = bin;

	return 0;
}

uint8_t mqtt_data_str_set(
	struct mqtt_data data[static 1], struct mqtt_str str[static 1])
{
	assert(data);
	assert(data->type == MQTT_DATA_STR_T);
	assert(str);

	data->data.str = str;

	return 0;
}

uint8_t mqtt_data_strpair_set(
	struct mqtt_data data[static 1], struct mqtt_strpair strpair[static 1])
{
	assert(data);
	assert(data->type == MQTT_DATA_STRPAIR_T);
	assert(strpair);

	data->data.strpair = strpair;

	return 0;
}
